<?php

namespace app\models;

class Renovations extends BaseModel
{
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'description' => 'Описание',
            'discount_cost' => 'Цена по скидке',
            'total_cost' => 'Цена',
            'image' => 'Изображения',
        ];
    }

    public static function label(){
        return 'Ремонтные работы';
    }

    public static function tableName(){
        return '{{%renovations}}';
    }
}
