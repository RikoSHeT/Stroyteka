<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class BaseModel extends ActiveRecord
{
    public function getImagePath()
    {
        return sprintf(
            "%s/%s/%s",
            Yii::$app->params['upload']['store'],
            \yii\helpers\StringHelper::basename(get_class($this)),
            $this->image
        );
    }

    public static function findByAlias($alias)
    {
        return self::find()->where(['alias' => $alias])->one();
    }

    /**
     * Получить удобоваримый массив записей для селекторов
     * @return array Массив всех записей вида id => name
     */
    public static function getList(){
        $rows = self::find()->all();

        $list = [];

        if (empty($rows)) {
            $list[0] = '- Нет -';
        } else {
            foreach ($rows as $value) {
                $list[$value->id] = $value->name;
            }
        }
        return $list;
    }
}
