<?php

use yii\helpers\Html;
use app\helpers\Helper;
use app\modules\admin\widgets\TreeView;
use kartik\cmenu\ContextMenu;

$this->title = $categories[0]::label();
?>
<section class="page">
    <br><br>
    <div class="container">
        <h1><?= Html::encode($this->title) ?></h1>

        <div class="col-xs-3 tree">
            <ul>
                <?php foreach ($categories as $category): ?>
                    <?= TreeView::widget([
                        'item' => $category,
                    ]); ?>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="category-form col-xs-9">
        </div>
    </div>
    <br><br>
</section>
