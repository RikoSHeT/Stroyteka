<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Gallery */

$this->title = $model->title;
?>
<section class="page">
    <?php
        $this->params['breadcrumbs'][] = ['label' => 'Галерея интерьеров', 'url' => ['index']];
        $this->params['breadcrumbs'][] = $this->title;
    ?>
    <br><br>
    <div class="container">
        <div class="gallery-view">

            <h1><?= Html::encode($this->title) ?></h1>
            <br>

            <p>
                <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'title',
                    'totalcost',
                    'square',
                    'projectcost',
                    'repaircost',
                    'finishcost',
                    'furniturecost',
                ],
            ]) ?>

        </div>
    </div>
    <br><br>
</section>
