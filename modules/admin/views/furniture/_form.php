<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Gallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(),[
        'editorOptions' => [
            'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false
        ],
    ]); ?>

    <?= $form->field($model, 'total_cost')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'image')->fileInput(); ?>

    <?php $images = $model->getImages(); ?>
    <?php foreach ($images as $image): ?>
        <img data-image-id = "<?= $image->id; ?>" src="<?= $image->getUrl('250x150'); ?>" alt="">
    <?php endforeach; ?>
    <br><br>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php $this->registerJS(sprintf(
        "$('[data-image-id]').on('dblclick', function(){
            var object = $(this);
            $.get('%s?id=' + $(this).data('imageId'), function(data){
                if (data) {
                    object.detach();
                }
            });
        })", Yii::$app->urlManager->createUrl(['admin/' . Yii::$app->controller->id . '/delete-image'])
    ), yii\web\View::POS_READY); ?>

</div>
