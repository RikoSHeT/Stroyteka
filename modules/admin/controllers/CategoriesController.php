<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Categories;
use app\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;

class CategoriesController extends BaseController
{

    public function mixinView($model, $data){

        $key = array_search('id_parent', $data);

        $data[$key] = [
            'value' => $model->parent ? $model->parent->name : 'Нет',
            'label' => 'Родительская категория',
        ];

        return $data;
    }

    public function actionIndex()
    {
        $categories = Categories::find()->andWhere(['depth' => 1])->all();

        return $this->render('index', [
            'categories' => $categories,
        ]);
    }

    public function createContextMenu($item)
    {
        $items = [];
        $items[] = ['label'=>'Создать дочернюю категорию', 'url' => '#', 'linkOptions' => [
            'data-create-category' => $item->id
            ]];

        if ($item->depth != 1) {
            $items[] = ['label'=>'Редактировать выбранную', 'url'=>'#', 'linkOptions' => [
                'data-edit-category' => $item->id,
            ]];

            $items[] = '<li class="divider"></li>';
            $items[] = ['label'=>'Удалить все', 'url'=> Url::to([$this->id . '/delete', 'id' => $item->id])];

        }
        return $items;
    }

    public function actionRenderForm($id_parent, $id = false){
        if (Yii::$app->request->isAjax) {

            if ($id) {
                $item = $this->findModel($id);
            } else {
                $item = $this->findModel(true, true);
            }

            if (!$item->id) {
                $item->id_parent = $id_parent;
            }

            echo $this->renderPartial('_form', [
                'model' => $item,
            ]);
        }
    }
}
