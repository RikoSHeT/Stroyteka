<?php
namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Interiors;
use app\components\BaseController;
use yii\web\NotFoundHttpException;

class InteriorsController extends BaseController {

    public function getIndexAttributes(){
        return [
            'id',
            'name',
            'description:html',
            'total_cost',
            'square',
            'projects_cost'
        ];
    }

    public function mixinView($model, $data)
    {
        $key = array_search('description', $data);

        $data[$key] = [
            'value' => $model->description,
            'format' => 'html',
            'label' => $model->getAttributeLabel('description'),
        ];

        return $data;
    }
}
