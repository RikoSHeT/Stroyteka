<?php

namespace app\helpers;

class Helper
{
    public static function generateRandomString($length = 6)
    {
        return substr( md5(rand()), 0, $length);
    }
}
