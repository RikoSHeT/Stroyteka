<?php

use yii\db\Migration;

class m161027_190059_alter_products_table extends Migration
{
    const TABLE_PRODUCTS = 'tbl_products';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_PRODUCTS, 'size', 'varchar(45)');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_PRODUCTS, 'size');
    }

}
