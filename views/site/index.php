<?php

/* @var $this yii\web\View */

$this->title = 'Главная страница';
?>


<section class="header" id="container-1">
    <div class="swiper-container">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <div class="swiper-slide" style="background-image:url('img/slider/sl1.jpg')"></div>
            <div class="swiper-slide" style="background-image:url('img/slider/sl2.jpg')"></div>
            <div class="swiper-slide" style="background-image:url('img/slider/sl1.jpg')"></div>
        </div>
        <div class="header--scroll">
            <i class="icon-scroll-bottom"></i>
            <p>Прокрутите вниз</p>
        </div>
    </div>
</section>

<section class="why" id="container-2">
    <div class="container">
        <div class="why--wrapper">
            <div class="why--wrapper-title">
                <h1>Почему мы</h1>
                <p>Мы даем полное понимание стоимости ремонта</p>
            </div>
            <div class="why--wrapper-items">
                <article class="item">
                    <div class="item--img">
                        <img src="img/coffe.png" alt="" class="img-responsive coffe">
                    </div>
                    <p>Готовые проекты</p>
                    <span>
                        Работая с нами, у вас нет необходимости
                        заказывать дизайн-проект.
                    </span>
                    <span>                      
                        Вы имеете возможность выбрать одно из
                        готовых решений.
                    </span>
                </article>
                <article class="item">
                    <div class="item--img">
                        <img src="img/furniture.png" alt="" class="img-responsive furniture">
                    </div>
                    <p>Материалы и мебель</p>
                    <span>
                        Наши дизайн-проекты идеально
                        сбалансированы по стоимости и качеству
                        подобранных материалов и мебели.​
                    </span>
                    <span>                      
                        Вам не потребуется посещать рынки и
                        магазины в поисках всего необходимого
                        для реализации проекта
                    </span>
                </article>
                <article class="item">
                    <div class="item--img">
                        <img src="img/building.png" alt="" class="img-responsive building">
                    </div>
                    <p>Ремонтные работы</p>
                    <span>
                        На Вашем объекте будут работать
                        сертифицированные мастера, 
                        специализирующееся на работе c 
                        материалами, используемые в наших
                        проетах.
                    </span>
                    <span>                      
                        Гарантия на работы составляет 2 года.
                    </span>
                </article>
            </div>
            <button href="#call-consult" class="button-consult">Заказать выезд консультанта</button>
        </div>
    </div>
</section>

<section class="info" id="container-3">
    <div class="container-fluid">
        <div class="col-md-6 col-sm-6 col-xs-12 info--catalog">
            <div class="info--catalog-wrapper">
                <div class="wrapper-title">
                    <p>Каталог</p>
                    <span>товаров</span>
                    <a href="<?= Yii::$app->urlManager->createUrl(['catalog/index']); ?>" class="go-catalog1">Перейти в каталог</a>
                </div>
            </div>
            <a href="<?= Yii::$app->urlManager->createUrl(['catalog/index']); ?>" class="ab-href1"></a>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 info--interior">
            <div class="info--catalog-wrapper right-wrapper">
                <div class="wrapper-title">
                    <p>Галерея</p>
                    <span>интерьеров</span>
                    <a href="<?= Yii::$app->urlManager->createUrl(['gallery/index']); ?>" class="go-catalog2">Перейти в галерею</a>
                </div>
            </div>
            <a href="<?= Yii::$app->urlManager->createUrl(['gallery/index']); ?>" class="ab-href2"></a>
        </div>
    </div>
</section>
