<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
// $this->params['breadcrumbs'][] = $this->title;
?>
    <section class="page">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="/">Главная</a></li>
                <li><a href="/contact.html">Контакты</a></li>
            </ul>
            <div class="row page--wrapper">
                <div class="col-xs-12 page--wrapper-contact">
                    <div class="page--wrapper-contact-title">
                        <h1>Контакты</h1>
                        <p>
                            По всем вопросам, связанным с работой компании и дополнительной информацией о товаре и                          услугах, обращайтесь по телефонам расположенным ниже, также вы можете
                            <a href="">написать нам.</a>
                        </p>
                    </div>
                    <div class="page--wrapper-contact-icons">
                        <div class="contact-items">
                            <i class="contact-phone"></i>
                            <div class="item-info">
                                <span>Телефон</span>
                                <p>8 800 123 45 67</p>
                            </div>
                        </div>
                        <div class="contact-items">
                            <i class="contact-mail"></i>
                            <div class="item-info">
                                <span>E-mail</span>
                                <a href="">sample@mail.ru</a>
                            </div>
                        </div>
                        <div class="contact-items">
                            <i class="contact-skype"></i>
                            <div class="item-info">
                                <span>Skype</span>
                                <p>sample-skype</p>
                            </div>
                        </div>
                        <div class="contact-items">
                            <i class="contact-adress"></i>
                            <div class="item-info">
                                <span>Адрес</span>
                                <p>г. Астана, ул. Ленина 22/3</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>