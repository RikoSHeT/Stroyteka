<?php foreach ($interiors as $item): ?>
    <article class="gallery-item">
        <div class="gallery-item--img">
            <a href="<?= $item->getImages()[0]->getUrl(); ?>" data-lightbox="roadtrip-1">
                <img src="<?= $item->getImages()[0]->getUrl() ?>"/>
            </a>
            <p><?= $item->total_cost; ?> тг</p>
            <a href="<?= Yii::$app->urlManager->createUrl(['interiors/view']); ?>" class="galery-cursor"></a>
        </div>
        <div class="gallery-item--title">
            <a href="<?= Yii::$app->urlManager->createUrl(['interiors/view' , 'alias' => $item->alias]); ?>">
                <?= $item->name; ?>
            </a>
            <div class="tags hidden">
                <a href="">Современный,</a>
                <a href="">Модернизм,</a>
                <a href="">Фьюжн</a>
            </div>
        </div>
    </article>
<?php endforeach; ?>
