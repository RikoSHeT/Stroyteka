<?php

/* @var $this yii\web\View */

$this->title = 'Каталог';
?>

<section class="page">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li><a href="/catalog.html">Каталог</a></li>
            <li><a href="/catalog-item.html">Название товара</a></li>
        </ul>
        <div class="row page--wrapper">
            <div class="col-md-6 col-xs-12 page--wrapper-gallery-left">
                <div class="page--wrapper-gallery-left--slider">
                    <ul id="imageGallery">
                        <li data-thumb="/img/gallery/thumb/gl1.jpg" data-src="/img/gallery/gl1.jpg">
                            <img src="/img/gallery/gl1.jpg" />
                        </li>
                        <li data-thumb="/img/gallery/thumb/gl1.jpg" data-src="/img/gallery/gl1.jpg">
                            <img src="/img/gallery/gl2.jpg" />
                        </li>
                        <li data-thumb="/img/gallery/thumb/gl1.jpg" data-src="/img/gallery/gl1.jpg">
                            <img src="/img/gallery/gl3.jpg" />
                        </li>
                        <li data-thumb="/img/gallery/thumb/gl1.jpg" data-src="/img/gallery/gl1.jpg">
                            <img src="/img/gallery/gl4.jpg" />
                        </li>
                        <li data-thumb="/img/gallery/thumb/gl1.jpg" data-src="/img/gallery/gl1.jpg">
                            <img src="/img/gallery/gl5.jpg" />
                        </li>
                        <li data-thumb="/img/gallery/thumb/gl1.jpg" data-src="/img/gallery/gl1.jpg">
                            <img src="/img/gallery/gl6.jpg" />
                        </li>
                        <li data-thumb="/img/gallery/thumb/gl1.jpg" data-src="/img/gallery/gl1.jpg">
                            <img src="/img/gallery/gl7.jpg" />
                        </li>
                        <li data-thumb="/img/gallery/thumb/gl1.jpg" data-src="/img/gallery/gl1.jpg">
                            <img src="/img/gallery/gl8.jpg" />
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 page--wrapper-catalog-right">
                <div class="page--wrapper-catalog-right--title">
                    <h1>Название товара</h1>
                </div>
                <div class="page--wrapper-catalog-right--info">
                    <ul>
                        <li>
                            <p>Наличие:</p>
                            <span>Есть в наличии</span>
                        </li>
                        <li>
                            <p>Код товара:</p>
                            <span>Т234622</span>
                        </li>
                        <li>
                            <p>Стиль:</p>
                            <span>Минимализм</span>
                        </li>
                        <li>
                            <p>Габариты:</p>
                            <span>1.2 м х 3 м</span>
                        </li>
                    </ul>
                </div>
                <div class="page--wrapper-catalog-right--price">
                    <div class="page--wrapper-catalog-right--price-total">
                        <span>95 000 тг</span>
                        <p>65 000 тг</p>
                    </div>
                    <div class="page--wrapper-catalog-right--price-buy">
                        <div class="input-number-box">
                            <input type="text" min="1" class="input-number" value="1">
                            <div class="btn input-number-more"></div>
                            <div class="btn input-number-less"></div>
                        </div>
                        <button href="#buy-modal" class="buy_btn">Купить</button>
                    </div>
                </div>
                <div class="page--wrapper-catalog-right--description">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aspernatur aut consectetur dolorem enim harum                      incidunt laborum natus, obcaecati praesentium provident quam rem ullam, veniam voluptate. Adipisci                      laboriosam maiores quidem.
                    </p>
                </div>
                <div class="page--wrapper-catalog-right--social">
                    <p>Поделиться</p>
                    <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                    <script src="//yastatic.net/share2/share.js"></script>
                    <div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,moimir,gplus" data-counter=""></div>
                </div>
            </div>
            <div class="col-xs-12 page--wrapper-gallery-more">
                <div class="page--wrapper-gallery-more--title">
                    <h3>Обратите внимание</h3>
                </div>
                <div class="page--wrapper-right-items-catalog">
                    <article class="catalog-item">
                        <div class="catalog-item--left">
                            <div class="flexslider c_slider">
                                <ul class="slides">
                                    <li>
                                        <a href="img/divan.jpg" data-lightbox="roadtrip" class="zoom-photo"><img
                                                src="img/divan.jpg"/></a>
                                    </li>
                                    <li>
                                        <a href="img/divan.jpg" data-lightbox="roadtrip"><img src="img/divan.jpg"/></a>
                                    </li>
                                    <li>
                                        <a href="img/divan.jpg" data-lightbox="roadtrip"><img src="img/divan.jpg"/></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="catalog-item--right">
                            <a href="/catalog-item.html">Дом с барельефами и витражами</a>
                            <div class="tags">
                                <a href="">Совремeнный,</a>
                                <a href="">Модернизм,</a>
                                <a href="">Фьюжн</a>
                            </div>
                            <span class="price">65 000 тенге</span>
                            <div class="flexslider c_carousel">
                                <ul class="slides">
                                    <li>
                                        <img src="img/divan.jpg"/>
                                    </li>
                                    <li>
                                        <img src="img/divan.jpg"/>
                                    </li>
                                    <li>
                                        <img src="img/divan.jpg"/>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </article>
                    <article class="catalog-item">
                        <div class="catalog-item--left">
                            <div class="flexslider c_slider">
                                <ul class="slides">
                                    <li>
                                        <a href="img/divan.jpg" data-lightbox="roadtrip"><img src="img/divan.jpg"/></a>
                                    </li>
                                    <li>
                                        <a href="img/divan.jpg" data-lightbox="roadtrip"><img src="img/divan.jpg"/></a>
                                    </li>
                                    <li>
                                        <a href="img/divan.jpg" data-lightbox="roadtrip"><img src="img/divan.jpg"/></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="catalog-item--right">
                            <a href="/catalog-item.html">Дом с барельефами и витражами</a>
                            <div class="tags">
                                <a href="">Современный,</a>
                                <a href="">Модернизм,</a>
                                <a href="">Фьюжн</a>
                            </div>
                            <span class="price">65 000 тенге</span>
                            <div class="flexslider c_carousel">
                                <ul class="slides">
                                    <li>
                                        <img src="img/divan.jpg"/>
                                    </li>
                                    <li>
                                        <img src="img/divan.jpg"/>
                                    </li>
                                    <li>
                                        <img src="img/divan.jpg"/>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</section>
