<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">

    <title><?= $this->title; ?></title>
    <meta name="description" content="">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Template Basic Images Start -->
    <meta property="og:image" content="path/to/image.jpg">
    <link rel="shortcut icon" href="/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="/img/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-touch-icon-114x114.png">
    <!-- Template Basic Images End -->

    <?php $this->head() ?>

    <!-- Custom Browsers Color Start -->
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">
    <!-- Custom Browsers Color End -->
    <!-- Custom Browsers Color End -->



</head>
<body>
<?php $this->beginBody() ?>
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>




    <div id="myNav" class="overlay">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <div class="overlay-content">
            <a href="<?= Yii::$app->urlManager->createUrl('interiors'); ?>">Галерея интерьеров</a>
            <a href="<?= Yii::$app->urlManager->createUrl('products'); ?>">Каталог товаров</a>
            <a href="<?= Yii::$app->urlManager->createUrl(['site/about']); ?>">О нас</a>
            <a href="<?= Yii::$app->urlManager->createUrl(['site/contacts']); ?>">Контакты</a>
        </div>
    </div>

    <header>
        <div class="container">
            <nav>
                <div class="header-left">
                    <a href="<?= Yii::$app->urlManager->createUrl(''); ?>">
                        <img src="/img/logo.png" alt="" class="img-responsive logo">
                    </a>
                </div>
                <div class="header-right">
                    <div class="header-right--social-menu">
                        <div class="header-right--social clearfix">
                            <ul class="social">
                                <li>
                                    <a href="">
                                        <i class="icon-vk"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <i class="icon-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <i class="icon-odnaklasniki"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="header-right--menu clearfix">
                            <ul>
                                <li><a href="<?= Yii::$app->urlManager->createUrl('interiors'); ?>">Галерея интерьеров</a></li>
                                <li><a href="<?= Yii::$app->urlManager->createUrl('products'); ?>">Каталог товаров</a></li>
                                <li><a href="<?= Yii::$app->urlManager->createUrl(['site/about']); ?>">О нас</a></li>
                                <li><a href="<?= Yii::$app->urlManager->createUrl(['site/contacts']); ?>">Контакты</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="header-right--call">
                        <p>
                            <i class="icon-phone"></i>
                                +7 800 123 45 67
                        </p>
                        <button href="#call-request" class="call-button">Обратная связь</button>
                    </div>
                </div>
            </nav>
        </div>
        <div class="nav-bottom visible-xs-block">
            <div class="container">
                <div class="nav-bottom-wrapper">
                    <ul class="social">
                        <li>
                            <a href="">
                                <i class="icon-vk"></i>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <i class="icon-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <i class="icon-odnaklasniki"></i>
                            </a>
                        </li>
                    </ul>
                    <a class="toggle-button visible-xs" onclick="openNav()"><span></span><span></span><span></span></a>
                </div>
            </div>
        </div>
    </header>

    <?= $content ?>

    <footer class="footer">
        <div class="container">
            <div class="footer--wrapper">
                <div class="footer--wrapper-left">
                    <ul class="footer--wrapper-left--menu">
                        <li><a href="<?= Yii::$app->urlManager->createUrl('catalog'); ?>">Каталог товаров</a></li>
                        <li><a href="<?= Yii::$app->urlManager->createUrl('interiors'); ?>">Галерея интерьеров</a></li>
                        <li><a href="<?= Yii::$app->urlManager->createUrl(['site/about']); ?>">О нас</a></li>
                        <li><a href="<?= Yii::$app->urlManager->createUrl(['site/contacts']); ?>">Контакты</a></li>
                    </ul>
                    <div class="footer--wrapper-left--copyright">
                        <span class="red-info">Стройтека</span>
                        <span>| Все права защищены</span>
                    </div>
                </div>
                <div class="footer--wrapper-right">
                    <ul class="social social-footer">
                        <li>
                            <a href="">
                                <i class="icon-vk"></i>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <i class="icon-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <i class="icon-odnaklasniki"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="footer--wrapper-right--copyright">
                        <span class="red-info">Создание сайта</span>
                        <span>| Creatie Design</span>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Call Modal -->
    <div id="call-request" class="mfp-hide call-request">
        <div class="call-request--title">
            <h1>Заказать звонок</h1>
        </div>
        <div class="call-request--content">
            <form action="">
                <div class="form-label">
                    <label for="name">Введите Ваши имя:</label>
                    <div class="form-label--input-name">
                        <input type="text" id="name" name="name" placeholder="Иван Иванов" required>
                    </div>
                </div>
                <div class="form-label">
                    <label for="phone">Введите Ваш телефон:</label>
                    <div class="form-label--input-phone">
                        <input type="tel" id="phone" class="phone" name="name" placeholder="+7(___)___-____" required>
                    </div>
                </div>
                <div class="form-label">
                    <button type="submit" class="form-call-button">Заказать звонок</button>
                </div>
                <p>
                    Ваши данные не будут переданны третьим лицам!
                </p>
            </form>
        </div>
    </div>


    <!-- Call Consult -->
    <div id="call-consult" class="mfp-hide call-request">
    <div class="call-request--title">
        <h1>Заказать выезд
            консультанта</h1>
    </div>
    <div class="call-request--content">
        <form action="">
            <div class="form-label">
                <label for="name">Введите Ваши имя:</label>
                <div class="form-label--input-name">
                    <input type="text" id="name" name="name" placeholder="Иван Иванов" required>
                </div>
            </div>
            <div class="form-label">
                <label for="phone">Введите Ваш телефон:</label>
                <div class="form-label--input-phone">
                    <input type="tel" id="phone" class="phone" name="name" placeholder="+7(___)___-____" required>
                </div>
            </div>
            <div class="form-label">
                <label for="email">Введите Ваш email:</label>
                <div class="form-label--input-mail">
                    <input type="email" id="email" name="email" placeholder="simple@mail.ru" required>
                </div>
            </div>
            <div class="form-label">
                <button type="submit" class="form-call-button">Заказать выезд</button>
            </div>
            <p>
                Ваши данные не будут переданны третьим лицам!
            </p>
        </form>
    </div>
</div>

    <?php $this->endBody() ?>

    <script>
        /* Open when someone clicks on the span element */
        function openNav() {
            document.getElementById("myNav").style.width = "100%";
        }

        /* Close when someone clicks on the "x" symbol inside the overlay */
        function closeNav() {
            document.getElementById("myNav").style.width = "0%";
        }
    </script>







</body>
</html>
<?php $this->endPage() ?>
