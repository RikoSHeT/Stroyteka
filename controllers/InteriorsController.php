<?php

namespace app\controllers;

use Yii;
use app\components\BaseController;
use app\models\Interiors;

class InteriorsController extends BaseController{

    public function actionIndex($filter = []){
        $interiors = Interiors::find()->orderBy("id DESC, name ASC")->all();

        return $this->render('index', [
            'interiors' => $interiors,
        ]);
    }

    public function actionView($alias)
    {
        $item = Interiors::findByAlias($alias);

        return $this->render('view', [
            'item' => $item,
        ]);
    }

    public function actionFilter($min = 0, $max = 0)
    {
        if (Yii::$app->request->isAjax) {
            $matches = Interiors::find()->orderBy("id DESC, name ASC")->where(['>','total_cost', (int)$min]);

            if (!empty($max)) {
                $matches = $matches->andWhere(['<','total_cost', (int)$max]);
            }

            $matches = $matches->all();

            echo $this->renderPartial('_item', [
                'interiors' => $matches,
            ]);
        }
    }
}
